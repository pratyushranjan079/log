@extends('layouts.master')


@section('title','User_Profile')

@section('head')

 <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">

    <link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" type="text/css">
    <link href="./assets/fonts/profession/style.css" rel="stylesheet" type="text/css">
    <!-- <link href="./assets/libraries/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"> -->
    <!-- <link href="./assets/libraries/bootstrap-fileinput/css/fileinput.min.css" rel="stylesheet" type="text/css"> -->
    <!-- <link href="./assets/libraries/bootstrap-select/css/bootstrap-select.min.css" rel="stylesheet" type="text/css"> -->
    <!-- <link href="./assets/libraries/bootstrap-wysiwyg/bootstrap-wysiwyg.min.css" rel="stylesheet" type="text/css"> -->
    <link href="./assets/css/profession-black-green.css" rel="stylesheet" type="text/css" id="style-primary">

    <link rel="shortcut icon" type="image/x-icon" href="assets/favicon.png">
    

@stop


@section('header')


    <!-- <div class="header-wrapper"> -->
    <!-- <div class="header"> -->
        <div class="header-top">
            <div class="container">
                <div class="header-brand">
                    <div class="header-logo">
                        <a href="index.html">
                            <i class="profession profession-logo"></i>
                            <span class="header-logo-text">Profession<span class="header-logo-highlight">.</span>portal</span>
                        </a>
                    </div><!-- /.header-logo-->

                    <div class="header-slogan">
                        <span class="header-slogan-slash">/</span>
                        <span class="header-slogan-text">Job Portal Template</span>
                    </div><!-- /.header-slogan-->
                </div><!-- /.header-brand -->

                <ul class="header-actions nav nav-pills">
                    <li><a href="/login">Login</a></li>
                    <li><a href="/register">Sign Up</a></li>
                    <li><a href="/create-resume" class="primary">Create Resume</a></li>
                </ul><!-- /.header-actions -->

                <button class="navbar-toggle collapsed" type="button" data-toggle="collapse" data-target=".header-nav">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div><!-- /.container -->
        </div><!-- /.header-top -->

        <div class="header-bottom">
            <div class="container">
                <ul class="header-nav nav nav-pills collapse">
                    <li class="active">
                        <a href="/">Home</a>
                    </li>

                    <li >
                        <a href="#">Companies <i class="fa fa-chevron-down"></i></a>

                        <ul class="sub-menu">
                            <li><a href="/companies">Company Listing</a></li>
                            <li><a href="/company-detail">Company Detail</a></li>
                        </ul>
                    </li>

                    <li >
                        <a href="#">Positions <i class="fa fa-chevron-down"></i> </a>
                        <ul class="sub-menu">
                            <li><a href="/positions">Position Listing</a></li>
                            <li><a href="/position-detail">Position Detail</a></li>
                        </ul>
                    </li>

                    <li >
                        <a href="#">Candidates <i class="fa fa-chevron-down"></i></a>

                        <ul class="sub-menu">
                            <li><a href="/candidates">Candidates List</a></li>
                            <li><a href="/resume">Resume</a></li>
                            <li><a href="/create-resume">Create Resume</a></li>
                        </ul><!-- /.sub-menu -->
                    </li>

                    <li >
                        <a href="#">Pages <i class="fa fa-chevron-down"></i></a>

                        <ul class="sub-menu">
                            <li><a href="/pricing">Pricing</a></li>
                            <li><a href="/login">Login</a></li>
                            <li><a href="/registration">Registration</a></li>
                        </ul>
                    </li>
                </ul>

                <div class="header-search hidden-sm">
                    <form method="get" action="?">
                        <input type="text" class="form-control" placeholder="Search ...">
                    </form>
                </div><!-- /.header-search -->
            </div><!-- /.container -->
        </div><!-- /.header-bottom -->
    <!-- </div>/.header -->
<!-- </div> /.header-wrapper -->
@stop

@section('content')

	@parent

<form action="" method="post"> 

<p>username: {{ $test->name }}</p>
<p>email:{{ $test->email}}</p>
<p>age:{{ $test->age}}</p>
<p>address:{{ $test->address}}</p>
<p>contact:{{ $test->contact}}</p>

<input type="hidden" name="id">

<a href="/edit_profile/{{ $test->id}} " ><input type="button" value="edit profile"></a>

<a href="/sessiondestroy"><input type="button" value="logout"></a>

</form>

@stop
																																									



































