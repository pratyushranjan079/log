<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Http\Controllers\Controller;

use DB;

use View;

use Input;

use Redirect;

use Session;

class themeController extends Controller
{
 	public function index(){

 		return view('pages.index');
 	}

 	public function login(){

 		return view('pages.logintheme');
 	}

 	public function login_store(Request $request){

		$email = $request->input('email');

       $password = $request->input('password');

       $query = DB::table('laravel5')->select('id')->where('email','=', $email)->get();

       // print_r($query);

       $testid = $query[0]->id;

       print_r($testid);
      
       if($query){

        Session::put('id', $testid);

        $value = Session::get('id');

        return $this->profile($request,$testid);
       


        // echo $value;
        // return redirect('profile');

       }else{

        return redirect('register');
       }
 	}

 	public function register(){

 		return view('pages.registration');
 	}


 	public function register_store(Request $request)
    {

        $email = $request->input('email');

        $username = $request->input('name');

        $password =$request->input('password');

        $age = $request->input('age');

        $address = $request->input('address');

        $contact = $request->input('contact');

        // $profile_pic = $request->input('profile_pic');
        
        // echo $email;

        // echo "\n";

        // echo $username;

        // echo "\n";

        // echo $password;

        // echo $age;

        // echo $address;

        // echo $contact;

    $id = DB::table('laravel5')->insert(['email' => $email, 'password' => $password, 'name' => $username, 'age' => $age, 'address' => $address, 'contact' => $contact]);
    
    $query_table = DB::table('laravel5')->select('*')->get();

            return view('pages.logintheme');


    }

 	 public function profile(Request $request,$id){

		$query1 = DB::table('laravel5')->select('*')->where('id','=',$id)->get();

        $test = array();

        return view::make('pages.profile')->with('test',$query1[0]);

    }

    // public function profile(){

    // 	// $prof_id = $prof[0];

    // 	// echo "printed";

    // 	return redirect('profile');
    // }

    public function edit_profile(Request $request,$id){

 
    $query_edit = DB::table('laravel5')->select('*')->where('id','=',$id)->get();

   	// print_r($query_edit);

    $test1 = array();

    return view::make('pages.edit_profile')->with('test1',$query_edit[0]);
    
    }

    public function showprofile(Request $request,$id){

    $age1 = $request->input('e_age');


    $address1 = $request->input('e_address');


    $contact1 = $request->input('e_contact');


    $query_update =  DB::table('laravel5')
            ->where('id','=', $id)
            ->update(['age' => $age1, 'address' => $address1, 'contact' => $contact1]);    

       // print_r($query_update);

        // print_r($id);

        return $this->profile($request,$id);
    
}

 	public function create_resume(){

 		return view('pages.create-resume');
 	}

 	public function resume(){

 		return view('pages.resume');
 	}

 	public function companies(){

 		return view('pages.companies');
 	}

 	public function company_detail(){

 		return view('pages.company-detail');

 	}

 	public function positions(){

 		return view('pages.positions');
 	}

 	public function position_detail(){

 		return view('pages.position-detail');
 	}

 	public function candidates_list(){

 		return view('pages.candidates');
 	}

 	public function pricing(){

 		return view('pages.pricing');
 	}

 	public function image_upload(){

 		return view('pages.image');

 	}

 	public function fileUpload(Request $request)
    {

        $imageTempName = $request->file('profile_pic')->getPathname();
      
        $imageName = $request->file('profile_pic')->getClientOriginalName();
      
        $path = base_path() . '/public/uploads/images/';
      
        $request->file('profile_pic')->move($path , $imageName);
      
        DB::table('laravel5')
            ->where('profile_pic', $imageTempName)
            ->update(['profile_pic' => $imageName]);

    }

 	 public function sessiondestroy(){

        session()->forget('id');

        return redirect('login');

    }
}
