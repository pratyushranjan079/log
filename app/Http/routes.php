<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });


Route::get('/','themeController@index');

Route::get ('/login','themeController@login');

Route::post('/login','themeController@login_store');

Route::get('/register','themeController@register');

Route::post('/register','themeController@register_store');

Route::get('/profile','themeController@login_store');

Route::post('/profile/{id}','themeController@showprofile');

Route::get('/edit_profile/{id}','themeController@edit_profile');

Route::post('/edit_profile/{id}','themeController@showprofile');

Route::post('/showprofile/{id}','themeController@showprofile');

Route::get('/showprofile/{id}','themeController@showprofile');

Route::get('/resume','themeController@resume');

Route::get('/create-resume','themeController@create_resume');

Route::get('/companies','themeController@companies');

Route::get('/company-detail','themeController@company_detail');

Route::get('/position-listing','themeController@positions');

Route::get('/position-detail','themeController@position_detail');

Route::get('/candidates-list','themeController@candidates_list');

Route::get('/pricing','themeController@pricing');

Route::get('/image','themeController@image_upload');

Route::post('/image','themeController@fileUpload');

Route::get('/sessiondestroy','themeController@sessiondestroy');

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () {
    //
});
