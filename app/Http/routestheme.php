<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/','loginformcontroller@index');

Route::get('/login','loginformcontroller@create');

Route::post('/login','loginformcontroller@login_store');

Route::get('/login_store','loginformcontroller@login_store');

Route::get('/register','loginformcontroller@register');

Route::post('/register','loginformcontroller@register_store');

Route::get('/edit_profile/{id}','loginformcontroller@edit_profile');

Route::post('/edit_profile','loginformcontroller@showprofile');

Route::post('/showprofile/{id}','loginformcontroller@showprofile');

Route::get('/profile/{id}','loginformcontroller@profile');

// Route::get('/logintheme',loginformcontroller@login)

Route::get('/sessiondestroy','loginformcontroller@sessiondestroy');

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () {
    //
});
